<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GaluhLibrary extends Facade {
   protected static function getFacadeAccessor() { return 'galuhlibrary'; }
}