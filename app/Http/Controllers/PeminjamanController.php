<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Classes\GaluhLibrary;
use DataTables;
use DB;

class PeminjamanController extends Controller
{
    public function page()
    {
        return view('pages.content.peminjaman');
    }

    public function getDataPeminjaman(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = DB::select("SELECT trx.id_transaksi,trx.id_anggota,trx.tgl_transaksi FROM tb_transaksi trx WHERE trx.deleted_at IS NULL");
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('anggota', function($row){
                        return GaluhLibrary::getNamaAnggota($row->id_anggota);
                    })
                    ->addColumn('tanggal_trx', function($row){
                        return Carbon::parse($row->tgl_transaksi)->format('d M Y');
                    })
                    ->addColumn('action', function($row){
                        $id = $row->id_transaksi;
                        $data = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
                        buku.judul,buku.pengarang,buku.cover
                        FROM tb_peminjaman pinjam
                        LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
                        WHERE pinjam.deleted_at IS NULL AND pinjam.id_transaksi = $id");
                        foreach ($data as $key => $value) {
                            if ($value->status == 'Dipinjam.') {
                                $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonTransaksiPinjam" data-name="modalTransaksiPinjam" data-type="addEdit" data-id="'.$row->id_transaksi.'" title="Edit Transaksi"><i class="la la-edit"></i></a>
                                              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonTransaksiPinjam" data-name="modalTransaksiPinjam" data-type="lihat" data-id="'.$row->id_transaksi.'" title="Lihat Transaksi"><i class="la la-eye"></i></a>
                                              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonTransaksiPinjam" data-name="modalTransaksiPinjam" data-type="pengembalian" data-id="'.$row->id_transaksi.'" title="Pengembalian Buku"><i class="la la-rotate-left"></i></a>
                                              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapusData" data-href="/admin/crud-trx-peminjaman/'.$row->id_transaksi.'" data-id="'.$row->id_transaksi.'" data-tbl="#tabel_transaksi_peminjaman" data-mtd="DELETE" title="Hapus Transaksi"><i class="la la-trash"></i></a>';
                            }else{
                                $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonTransaksiPinjam" data-name="modalTransaksiPinjam" data-type="lihat" data-id="'.$row->id_transaksi.'" title="Lihat Transaksi"><i class="la la-eye"></i></a>';
                            }
                            return $actionBtn;
                        }
                    })
                    ->rawColumns(['anggota','tgl_transaksi','action'])->make(true);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function crudPeminjaman(Request $request)
    {
        try {
            if ($request->isMethod('POST')) {
                $request->validate([
                    'anggota'            => 'required',
                    'tanggal_transaksi'  => 'required',
                    'tgl_jatuh_tempo'    => 'required',
                ],[
                    'anggota.required'            => 'Nama Anggota Tidak Boleh Kosong.',
                    'tanggal_transaksi.required'  => 'Tanggal Transaksi Tidak Boleh Kosong',
                    'tgl_jatuh_tempo.required'    => 'Tanggal Jatuh Tempo Tidak Boleh Kosong',
                ]);

                $dbTransaction = DB::transaction(function() use($request) {
                    $transaksi = [
                        'id_anggota'       => $request->anggota,
                        'id_user'          => 1,
                        'tgl_transaksi'    => Carbon::parse($request->tgl_tansaksi)->format('Y-m-d'),
                        'tgl_jatuh_tempo'  => Carbon::parse($request->tgl_jatuh_tempo)->format('Y-m-d'),
                        'created_at'       => Carbon::now()
                    ];
                    DB::table('tb_transaksi')->insert($transaksi);
                    $id_transaksi  = DB::getPdo()->lastInsertId();

                    if(isset($request->id_buku) === true) {
                        foreach ($request->id_buku as $value) {
                            $peminjaman = [
                                'id_transaksi'     => $id_transaksi,
                                'id_buku'          => $value,
                                'tgl_pinjam'       => Carbon::parse($request->tgl_tansaksi)->format('Y-m-d'),
                                'status'           => 'Dipinjam.',
                                'created_at'       => Carbon::now()
                            ];
                            DB::table('tb_peminjaman')->insert($peminjaman);
                        }
                    }
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Data Transaksi berhasil di tambahkan.'
                ]);
            }

            if ($request->isMethod('PUT')) {
                $request->validate([
                    'anggota'            => 'required',
                    'tanggal_transaksi'  => 'required',
                    'tgl_jatuh_tempo'    => 'required',
                ],[
                    'anggota.required'            => 'Nama Anggota Tidak Boleh Kosong.',
                    'tanggal_transaksi.required'  => 'Tanggal Transaksi Tidak Boleh Kosong',
                    'tgl_jatuh_tempo.required'    => 'Tanggal Jatuh Tempo Tidak Boleh Kosong',
                ]);

                $dbTransaction = DB::transaction(function() use($request) {
                    $transaksi = [
                        'id_anggota'       => $request->anggota,
                        'id_user'          => 1,
                        'tgl_transaksi'    => Carbon::parse($request->tgl_tansaksi)->format('Y-m-d'),
                        'tgl_jatuh_tempo'  => Carbon::parse($request->tgl_jatuh_tempo)->format('Y-m-d'),
                        'created_at'       => Carbon::now()
                    ];
                    DB::table('tb_transaksi')->where('id_transaksi', $request->id_transaksi)->update($transaksi);

                    DB::table('tb_peminjaman')->where('id_transaksi', $request->id_transaksi)->delete();
                    
                    if(isset($request->id_buku) === true) {
                        foreach ($request->id_buku as $value) {
                            $peminjaman = [
                                'id_transaksi'     => $request->id_transaksi,
                                'id_buku'          => $value,
                                'tgl_pinjam'       => Carbon::parse($request->tgl_tansaksi)->format('Y-m-d'),
                                'status'           => 'Dipinjam.',
                                'created_at'       => Carbon::now()
                            ];
                            DB::table('tb_peminjaman')->insert($peminjaman);
                        }
                    }
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Data Transaksi berhasil di edit.'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function pengembalian(Request $request)
    {
        try {
            $request->validate([
                'tgl_kembali' => 'required',
            ],[
                'tgl_kembali.required' => 'Tanggal Kembali Tidak Boleh Kosong.',
            ]);


            $dbTransaction = DB::transaction(function() use($request) {
                $id = $request->id_transaksi;
                $data = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
                buku.judul,buku.pengarang,buku.cover
                FROM tb_peminjaman pinjam
                LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
                WHERE pinjam.deleted_at IS NULL AND pinjam.id_transaksi = $id");

                foreach ($data as $key => $value) {
                    $pengembalian = [
                        'id_pinjam'     => $value->id_pinjam,
                        'tgl_kembali'   => Carbon::parse($request->tgl_kembali)->format('Y-m-d'),
                        'created_at'    => Carbon::now()
                    ];
                    DB::table('tb_pengembalian')->insert($pengembalian);

                    $pinjam = [
                        'status' => 'Dikembalikan.'
                    ];
                    DB::table('tb_peminjaman')->where('id_transaksi', $id)->update($pinjam);
                }
            });

            return response()->json([
                'status'    => 200,
                'message'   => 'Buku berhasil dikembalikan.'
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function pencarianBuku(Request $request)
    {
        $cariBuku = $request->cariBuku;

        $buku = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas, kat.nama_kat,kat.id_kategori, lok.kode_lokasi,lok.loker,lok.id_lokasi
        FROM tb_buku buku
        LEFT JOIN tb_kategori kat ON kat.id_kategori = buku.id_kategori
        LEFT JOIN tb_lokasi lok ON lok.id_lokasi = buku.id_lokasi
        WHERE buku.deleted_at IS NULL AND buku.judul LIKE '%$cariBuku%'");

        $cover = [];
        foreach ($buku as $item) {
            $cover[] = asset('storage/cover-book/'.$item->cover);
        }

        return response()->json([
            'data'  => $buku,
            'cover' => $cover
        ]);
    }
}
