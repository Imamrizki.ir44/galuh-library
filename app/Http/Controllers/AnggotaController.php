<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AnggotaModel;
use DataTables;

class AnggotaController extends Controller
{
    public function page()
    {
        return view('pages.content.anggota');
    }

    public function tabelAnggota(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = AnggotaModel::where('deleted_at', NULL)->orderBy('created_at', 'desc')->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonAnggota" data-name="modalAnggota" data-id="'.$row->id_anggota.'" title="Edit Anggota"><i class="la la-edit"></i></a>
                                      <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapusData" data-href="/admin/crud-anggota/'.$row->id_anggota.'" data-id="'.$row->id_anggota.'" data-tbl="#tabel_anggota" data-mtd="DELETE" title="Hapus Anggota"><i class="la la-trash"></i></a>';
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])->make(true);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function crudAnggota(Request $request)
    {
        if (!$request->isMethod('DELETE')) {
            $request->validate([
                'nama'      => 'required',
                'pekerjaan' => 'required',
                'no_hp'     => 'required',
            ],[
                'nama.required'      => 'Nama Anggota Tidak Boleh Kosong.',
                'pekerjaan.required' => 'Pekerjaan Tidak Boleh Kosong.',
                'no_hp.required'     => 'Nomor Hp Tidak Boleh Kosong.'
            ]);
        }

        try {
            if ($request->isMethod('POST')) {
                
                $anggota = AnggotaModel::create([
                    'nama'      => $request->nama,
                    'pekerjaan' => $request->pekerjaan,
                    'no_hp'     => $request->no_hp
                ]);

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Anggota berhasil di tambahkan.'
                ]);
            }

            if ($request->isMethod('PUT')) {
                $anggota = AnggotaModel::where('id_anggota', $request->id_anggota)->update([
                    'nama'      => $request->nama,
                    'pekerjaan' => $request->pekerjaan,
                    'no_hp'     => $request->no_hp
                ]);

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Anggota berhasil di edit.'
                ]);
            }

            if ($request->isMethod('DELETE')) {
                $anggota = AnggotaModel::find($request->id);
                $anggota->delete();

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Anggota berhasil di hapus.'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
