<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use DB;

class BukuController extends Controller
{
    public function page()
    {
        return view('pages.content.buku');
    }

    // Buku Modul
    public function tabelBuku(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas, kat.nama_kat, lok.kode_lokasi,lok.loker
                FROM tb_buku buku
                LEFT JOIN tb_kategori kat ON kat.id_kategori = buku.id_kategori
                LEFT JOIN tb_lokasi lok ON lok.id_lokasi = buku.id_lokasi
                WHERE buku.deleted_at IS NULL");
                
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('cover', function($row){
                        $url_cover = storage_path('app/public/cover-book/'.$row->cover);
                        $cover = asset('assets/media/books/default_cover.png');
                        if(is_file($url_cover) == true){
                            $cover = asset('storage/cover-book/'.$row->cover);
                        }
                        return '<img width="60%" src="'.$cover.'"/>';
                    })
                    ->addColumn('lokasi', function($row){
                        return $row->kode_lokasi.' - '.$row->loker;
                    })
                    ->addColumn('action', function($row){
                        $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonBuku" data-name="modalBuku" data-id="'.$row->id_buku.'" title="Edit Buku"><i class="la la-edit"></i></a>
                                      <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapusData" data-href="/admin/crud-buku/'.$row->id_buku.'" data-id="'.$row->id_buku.'" data-tbl="#tabel_buku" data-mtd="DELETE" title="Hapus Buku"><i class="la la-trash"></i></a>';
                        return $actionBtn;
                    })
                    ->rawColumns(['cover','lokasi','action'])->make(true);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function crudBuku(Request $request)
    {
        $id = $request->id_buku;
        $now = Carbon::now();
        try {
            if ($request->isMethod('POST')) {
                $request->validate([
                    'judul'     => 'required|unique:tb_buku,judul,'.$id.',id_buku,deleted_at,NULL',
                    'pengarang' => 'required',
                    'kategori'  => 'required',
                    'lokasi'    => 'required',
                ],[
                    'judul.required'     => 'Judul Buku Tidak Boleh Kosong.',
                    'judul.unique'       => 'Judul Buku Sudah Ada.',
                    'pengarang.required' => 'Pengarang Tidak Boleh Kosong.',
                    'kategori.required'  => 'Kategori Tidak Boleh Kosong.',
                    'lokasi.required'    => 'Lokasi Buku Tidak Boleh Kosong.',
                ]);

                $file_name = null;
                if ($id == null) {
                    if ($request->file('cover') != null) {
                        $file_ext  = $request->file('cover')->getClientOriginalExtension();
                        $file_name = time().'_'.$request->judul.'.'.$file_ext;
                        $file = $request->file('cover')->storeAs(
                            'public/cover-book', time().'_'.$request->judul.'.'.$file_ext
                        );
                    }

                    $message = 'Buku berhasil di tambahkan.';
                    $dbTransaction = DB::transaction(function() use($request, $file_name, $now) {
                        $buku = [
                            'judul'       => $request->judul,
                            'pengarang'   => $request->pengarang,
                            'id_kategori' => $request->kategori,
                            'id_lokasi'   => $request->lokasi,
                            'kuantitas'   => $request->kuantitas,
                            'cover'       => $file_name,
                            'created_at'  => $now
                        ];
                        DB::table('tb_buku')->insert($buku);
                    });
                }else{
                    $buku = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas, kat.nama_kat, lok.kode_lokasi,lok.loker
                    FROM tb_buku buku
                    LEFT JOIN tb_kategori kat ON kat.id_kategori = buku.id_kategori
                    LEFT JOIN tb_lokasi lok ON lok.id_lokasi = buku.id_lokasi
                    WHERE buku.deleted_at IS NULL AND buku.id_buku = $id")[0];
                    $url_cover = storage_path('app/public/cover-book/'.$buku->cover);
                    if(is_file($url_cover) == true){
                        unlink($url_cover);
                    }

                    if ($request->file('cover') != null) {
                        $file_ext  = $request->file('cover')->getClientOriginalExtension();
                        $file_name = time().'_'.$request->judul.'.'.$file_ext;
                        $file = $request->file('cover')->storeAs(
                            'public/cover-book', time().'_'.$request->judul.'.'.$file_ext
                        );
                    }

                    $message = 'Buku berhasil di edit.';
                    $dbTransaction = DB::transaction(function() use($request, $file_name, $now) {
                        $buku = [
                            'judul'       => $request->judul,
                            'pengarang'   => $request->pengarang,
                            'id_kategori' => $request->kategori,
                            'id_lokasi'   => $request->lokasi,
                            'kuantitas'   => $request->kuantitas,
                            'cover'       => $file_name,
                            'created_at'  => $now
                        ];
                        DB::table('tb_buku')->where('id_buku', $request->id_buku)->update($buku);
                    });
                }

                return response()->json([
                    'status'    => 200,
                    'message'   => $message
                ]);
            }

            if ($request->isMethod('DELETE')) {
                $id_delete = $request->id;
                $buku = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas
                FROM tb_buku buku WHERE buku.deleted_at IS NULL AND buku.id_buku = $id_delete")[0];
                $url_cover = storage_path('app/public/cover-book/'.$buku->cover);
                if(is_file($url_cover) == true){
                    unlink($url_cover);
                }

                $now = Carbon::now();
                $dbTransaction = DB::transaction(function() use($request, $now) {
                    $buku = [
                        'deleted_at' => $now
                    ];
                    DB::table('tb_buku')->where('id_buku', $request->id)->update($buku);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Buku berhasil di hapus.'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Kategori Modul
    public function tabelKategori(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = DB::select("SELECT * FROM tb_kategori WHERE deleted_at IS NULL ORDER BY created_at DESC");
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonKategori" data-name="modalKategori" data-id="'.$row->id_kategori.'" title="Edit Kategori"><i class="la la-edit"></i></a>
                                      <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapusData" data-href="/admin/crud-kategori/'.$row->id_kategori.'" data-id="'.$row->id_kategori.'" data-tbl="#tabel_kategori" data-mtd="DELETE" title="Hapus Kategori"><i class="la la-trash"></i></a>';
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])->make(true);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function crudKategori(Request $request)
    {
        $id = $request->id_kategori;
        try {
            if ($request->isMethod('POST')) {
                $request->validate([
                    'nama_kat'      => 'required|unique:tb_kategori,nama_kat,NULL,id_kategori,deleted_at,NULL',
                ],[
                    'nama_kat.required' => 'Nama Kategori Tidak Boleh Kosong.',
                    'nama_kat.unique'   => 'Nama Kategori Sudah Ada.',
                ]);
                
                $dbTransaction = DB::transaction(function() use($request) {
                    $kategori = [
                        'nama_kat' => $request->nama_kat
                    ];
                    DB::table('tb_kategori')->insert($kategori);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Kategori berhasil di tambahkan.'
                ]);
            }

            if ($request->isMethod('PUT')) {
                $request->validate([
                    'nama_kat'      => 'required|unique:tb_kategori,nama_kat,'.$id.',id_kategori,deleted_at,NULL',
                ],[
                    'nama_kat.required' => 'Nama Kategori Tidak Boleh Kosong.',
                    'nama_kat.unique'   => 'Nama Kategori Sudah Ada.',
                ]);
                
                $dbTransaction = DB::transaction(function() use($request) {
                    $kategori = [
                        'nama_kat' => $request->nama_kat
                    ];
                    DB::table('tb_kategori')->where('id_kategori', $request->id_kategori)->update($kategori);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Kategori berhasil di edit.'
                ]);
            }

            if ($request->isMethod('DELETE')) {
                $now = Carbon::now();
                $dbTransaction = DB::transaction(function() use($request, $now) {
                    $kategori = [
                        'deleted_at' => $now
                    ];
                    DB::table('tb_kategori')->where('id_kategori', $request->id)->update($kategori);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Kategori berhasil di hapus.'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // Lokasi Modul
    public function tabelLokasi(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = DB::select("SELECT * FROM tb_lokasi WHERE deleted_at IS NULL ORDER BY kode_lokasi ASC");
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $actionBtn = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon buttonLokasi" data-name="modalLokasi" data-id="'.$row->id_lokasi.'" title="Edit Lokasi"><i class="la la-edit"></i></a>
                                      <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapusData" data-href="/admin/crud-lokasi/'.$row->id_lokasi.'" data-id="'.$row->id_lokasi.'" data-tbl="#tabel_lokasi" data-mtd="DELETE" title="Hapus Lokasi"><i class="la la-trash"></i></a>';
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])->make(true);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function crudLokasi(Request $request)
    {
        $id = $request->id_lokasi;
        try {
            if ($request->isMethod('POST')) {
                $request->validate([
                    'kode_lokasi' => 'required',
                    'loker'       => 'required',
                ],[
                    'kode_lokasi.required' => 'Kode Lokasi Tidak Boleh Kosong.',
                    'kode_lokasi.unique'   => 'Kode Lokasi Sudah Ada.',
                ]);
                
                $dbTransaction = DB::transaction(function() use($request) {
                    $lokasi = [
                        'kode_lokasi' => $request->kode_lokasi,
                        'loker'    => $request->loker
                    ];
                    DB::table('tb_lokasi')->insert($lokasi);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Lokasi berhasil di tambahkan.'
                ]);
            }

            if ($request->isMethod('PUT')) {
                $request->validate([
                    'kode_lokasi' => 'required',
                    'loker'       => 'required',
                ],[
                    'kode_lokasi.required' => 'Kode Lokasi Tidak Boleh Kosong.',
                    'kode_lokasi.unique'   => 'Kode Lokasi Sudah Ada.',
                ]);
                
                $dbTransaction = DB::transaction(function() use($request) {
                    $lokasi = [
                        'kode_lokasi' => $request->kode_lokasi,
                        'loker'    => $request->loker
                    ];
                    DB::table('tb_lokasi')->where('id_lokasi', $request->id_lokasi)->update($lokasi);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Lokasi berhasil di edit.'
                ]);
            }

            if ($request->isMethod('DELETE')) {
                $now = Carbon::now();
                $dbTransaction = DB::transaction(function() use($request, $now) {
                    $lokasi = [
                        'deleted_at' => $now
                    ];
                    DB::table('tb_lokasi')->where('id_lokasi', $request->id)->update($lokasi);
                });

                return response()->json([
                    'status'    => 200,
                    'message'   => 'Lokasi berhasil di hapus.'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    
}
