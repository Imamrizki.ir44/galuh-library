<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function page()
    {
        $data = DB::select("SELECT trx.id_transaksi,trx.id_anggota,trx.tgl_transaksi,trx.tgl_jatuh_tempo FROM tb_transaksi trx WHERE trx.deleted_at IS NULL");
        return view('pages.content.dashboard', ['data' => $data]);
    }

    public function dataPinjam()
    {
        try {
            $dataPinjam = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
            buku.judul,buku.pengarang,buku.cover
            FROM tb_peminjaman pinjam
            LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
            WHERE pinjam.deleted_at IS NULL");

            $pengembalian = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
            buku.judul,buku.pengarang,buku.cover
            FROM tb_peminjaman pinjam
            LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
            WHERE pinjam.deleted_at IS NULL AND pinjam.status = 'Dikembalikan.'");

            $peminjaman = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
            buku.judul,buku.pengarang,buku.cover
            FROM tb_peminjaman pinjam
            LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
            WHERE pinjam.deleted_at IS NULL AND pinjam.status = 'Dipinjam.'");

            $data  = array();

            if (count($dataPinjam) != 0 || count($pengembalian) != 0 || count($peminjaman) != 0) {
                $data[0]['kembali'] = round(count($pengembalian) / (count($dataPinjam) / 100),2);
                $data[1]['pinjam'] = round(count($peminjaman) / (count($dataPinjam) / 100),2);
            }else{
                $data[0]['kembali'] = 0;
                $data[1]['pinjam'] = 0;
            }

            return response()->json([
                'status'  => 200,
                'data'    => $data
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
