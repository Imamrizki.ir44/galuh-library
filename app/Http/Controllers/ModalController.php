<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AnggotaModel;
use App\Classes\GaluhLibrary;
use Carbon\Carbon;
use DB;

class ModalController extends Controller
{
    public function componentModal(Request $request)
    {
        switch ($request->dataName) {
            case 'modalAnggota':
                if ($request->dataId != 0) {
                    $method = 'PUT';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-edit"></i> Edit Anggota</span>';
                    $data = AnggotaModel::where('id_anggota', $request->dataId)->first();
                }else{
                    $method = 'POST';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-plus"></i> Tambah Anggota</span>';

                }
                $modal_size   = 'modal-lg';
                $modal_header = $title;
                $modal_body   = '
                    <input type="hidden" name="id_anggota" value="'.($request->dataId != 0 ? $data->id_anggota : '').'">
                    <div class="form-group">
                        <label class="form-control-label">Nama Anggota <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="nama" value="'.($request->dataId != 0 ? $data->nama : '').'" maxlength="50" placeholder="Masukan Nama Anggota"/>
                        <div class="invalid-feedback" id="nama"></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Pekerjaan <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="pekerjaan" value="'.($request->dataId != 0 ? $data->pekerjaan : '').'" maxlength="50" placeholder="Masukan Pekerjaan"/>
                        <div class="invalid-feedback" id="pekerjaan"></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Nomor Hp <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="no_hp" value="'.($request->dataId != 0 ? $data->no_hp : '').'" maxlength="20" placeholder="Masukan Nomor Hp"/>
                        <div class="invalid-feedback" id="no_hp"></div>
                    </div>
                ';
                $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/crud-anggota/'.$request->dataId.'" data-tbl="#tabel_anggota" data-mtd="'.$method.'"><i class="fas fa-save"></i> Simpan</button>
                                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';

                $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                
                return response()->json($data);
            break;

            case 'modalKategori':
                $id = $request->dataId;
                if ($request->dataId != 0) {
                    $method = 'PUT';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-edit"></i> Edit Kategori</span>';
                    $data = DB::select("SELECT * FROM tb_kategori WHERE id_kategori = $id AND deleted_at IS NULL")[0];
                }else{
                    $method = 'POST';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-plus"></i> Tambah Kategori</span>';

                }
                $modal_size   = 'modal-sm';
                $modal_header = $title;
                $modal_body   = '
                    <input type="hidden" name="id_kategori" value="'.($request->dataId != 0 ? $data->id_kategori : '').'">
                    <div class="form-group">
                        <label class="form-control-label">Nama Kategori <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="nama_kat" value="'.($request->dataId != 0 ? $data->nama_kat : '').'" maxlength="50" placeholder="Masukan Kategori"/>
                        <div class="invalid-feedback" id="nama_kat"></div>
                    </div>
                ';
                $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/crud-kategori/'.$request->dataId.'" data-tbl="#tabel_kategori" data-mtd="'.$method.'"><i class="fas fa-save"></i> Simpan</button>
                                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';

                $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                
                return response()->json($data);
            break;

            case 'modalLokasi':
                $id = $request->dataId;
                if ($request->dataId != 0) {
                    $method = 'PUT';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-edit"></i> Edit Lokasi</span>';
                    $data = DB::select("SELECT * FROM tb_lokasi WHERE id_lokasi = $id AND deleted_at IS NULL")[0];
                }else{
                    $method = 'POST';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-plus"></i> Tambah Lokasi</span>';

                }
                $modal_size   = 'modal-sm';
                $modal_header = $title;
                $modal_body   = '
                    <input type="hidden" name="id_lokasi" value="'.($request->dataId != 0 ? $data->id_lokasi : '').'">
                    <div class="form-group">
                        <label class="form-control-label">Kode Lokasi <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="kode_lokasi" value="'.($request->dataId != 0 ? $data->kode_lokasi : '').'" maxlength="10" placeholder="Masukan Kode Lokasi"/>
                        <div class="invalid-feedback" id="kode_lokasi"></div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Loker <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="loker" value="'.($request->dataId != 0 ? $data->loker : '').'" maxlength="15" placeholder="Masukan Kode Lokasi"/>
                        <div class="invalid-feedback" id="loker"></div>
                    </div>
                ';
                $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/crud-lokasi/'.$request->dataId.'" data-tbl="#tabel_lokasi" data-mtd="'.$method.'"><i class="fas fa-save"></i> Simpan</button>
                                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';

                $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                
                return response()->json($data);
            break;

            case 'modalBuku':
                $id = $request->dataId;
                $file_cover = '';
                $method = 'POST';
                $kategori = null;
                $lokasi = null;
                if ($request->dataId != 0) {
                    $title = '<i class="kt-font-brand modal_icon flaticon2-edit"></i> Edit Buku</span>';
                    $data = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas, kat.nama_kat,kat.id_kategori, lok.kode_lokasi,lok.loker,lok.id_lokasi
                    FROM tb_buku buku
                    LEFT JOIN tb_kategori kat ON kat.id_kategori = buku.id_kategori
                    LEFT JOIN tb_lokasi lok ON lok.id_lokasi = buku.id_lokasi
                    WHERE buku.deleted_at IS NULL AND id_buku = $id")[0];
                    $file_cover = $data->cover;
                    $kategori = $data->id_kategori;
                    $lokasi = $data->id_lokasi;
                }else{
                    $title = '<i class="kt-font-brand modal_icon flaticon2-plus"></i> Tambah Buku</span>';
                }
                $modal_size   = 'modal-lg';
                $modal_header = $title;
                $modal_body   = '
                    <input type="hidden" name="id_buku" value="'.($request->dataId != 0 ? $data->id_buku : '').'">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Judul Buku <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="judul" value="'.($request->dataId != 0 ? $data->judul : '').'" maxlength="50" placeholder="Masukan Judul Buku"/>
                                <div class="invalid-feedback" id="judul"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Pengarang <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="pengarang" value="'.($request->dataId != 0 ? $data->pengarang : '').'" maxlength="30" placeholder="Masukan Pengarang"/>
                                <div class="invalid-feedback" id="pengarang"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Kategori Buku <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="kategori">
                                    <option value="" selected disabled>Pilih Kategori</option>
                                    '.GaluhLibrary::dropdownKategori($kategori).'
                                </select>
                                <div class="invalid-feedback" id="kategori"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Lokasi Buku <span class="text-danger">*</span></label>
                                <select class="form-control select2" name="lokasi">
                                    <option value="" selected disabled>Pilih Kode Lokasi - Loker</option>
                                    '.GaluhLibrary::dropdownLokasi($lokasi).'
                                </select>
                                <div class="invalid-feedback" id="lokasi"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Kuantitas</label>
                                <input type="number" class="form-control" name="kuantitas" value="'.($request->dataId != 0 ? $data->kuantitas : '').'" maxlength="10" placeholder="Masukan Kuantitas"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Upload Cover</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="cover" accept=".jpg, .jpeg, .png">
                                    <label class="custom-file-label">Pilih File</label>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
                $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/crud-buku/'.$request->dataId.'" data-tbl="#tabel_buku" data-mtd="'.$method.'"><i class="fas fa-save"></i> Simpan</button>
                                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';

                $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer, 'file_cover' => $file_cover);
                
                return response()->json($data);
            break;

            case 'modalTransaksiPinjam':
                $id = $request->dataId;
                $getIdAnggota = '';
                if ($request->dataId != 0) {
                    $method = 'PUT';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-edit"></i> Edit Transaksi Peminjaman</span>';
                    $data = DB::select("SELECT trx.id_transaksi,trx.id_anggota,trx.tgl_transaksi,trx.tgl_jatuh_tempo FROM tb_transaksi trx WHERE trx.deleted_at IS NULL AND trx.id_transaksi = $id")[0];
                    $getIdAnggota = $data->id_anggota;
                }else{
                    $method = 'POST';
                    $title = '<i class="kt-font-brand modal_icon flaticon2-plus"></i> Tambah Transaksi Peminjaman</span>';
                }

                if ($request->dataType == 'addEdit') {
                    $modal_size   = 'modal-full';
                    $modal_header = $title;
                    $modal_body   = '
                        <input type="hidden" name="id_transaksi" value="'.$id.'">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Nama Anggota <span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="anggota">
                                        <option value="" selected disabled>Pilih Nama Anggota</option>
                                        '.GaluhLibrary::dropdownAnggota($getIdAnggota).'
                                    </select>
                                    <div class="invalid-feedback" id="anggota"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Transaksi Pinjam <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control date" readonly placeholder="Pilih Tanggal Transaksi" name="tanggal_transaksi" value="'.($request->dataId != 0 ? Carbon::parse($data->tgl_transaksi)->format('d M Y') : '').'">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                        <div class="invalid-feedback" id="tanggal_transaksi"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Jatuh Tempo <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control date" readonly placeholder="Pilih Tanggal Jatuh Tempo" name="tgl_jatuh_tempo" value="'.($request->dataId != 0 ? Carbon::parse($data->tgl_jatuh_tempo)->format('d M Y') : '').'">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                        <div class="invalid-feedback" id="tgl_jatuh_tempo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed mb-6"></div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" maxlength="50" id="pencarianBuku" placeholder="Cari Judul Buku"/>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="rowSearchResult">
                            '.GaluhLibrary::getDataBuku($id).'
                        </div>
    
                    ';
                    $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/crud-trx-peminjaman/'.$request->dataId.'" data-tbl="#tabel_transaksi_peminjaman" data-mtd="'.$method.'"><i class="fas fa-save"></i> Simpan</button>
                                     <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';
    
                    $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                }
                elseif ($request->dataType == 'lihat'){
                    $modal_size   = 'modal-full';
                    $modal_header = '<i class="kt-font-brand modal_icon flaticon-eye"></i> Lihat Transaksi Peminjaman</span>';
                    $modal_body   = '
                        <input type="hidden" name="id_transaksi" value="'.$id.'">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Nama Anggota</label><br>
                                    <strong>'.GaluhLibrary::getNamaAnggota($getIdAnggota).'</strong>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Transaksi Pinjam</label><br>
                                    <strong>'.($request->dataId != 0 ? Carbon::parse($data->tgl_transaksi)->format('d M Y') : '').'</strong>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Jatuh Tempo</label><br>
                                    <strong>'.($request->dataId != 0 ? Carbon::parse($data->tgl_jatuh_tempo)->format('d M Y') : '').'</strong>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            '.GaluhLibrary::getDataBukuView($id).'
                        </div>
                    ';
                    $modal_footer = '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';
    
                    $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                }
                else{
                    $modal_size   = 'modal-sm';
                    $modal_header = '<i class="kt-font-brand modal_icon flaticon-reply"></i> Pengembalian Buku</span>';
                    $modal_body   = '
                        <input type="hidden" name="id_transaksi" value="'.$id.'">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Kembali <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control date" readonly placeholder="Pilih Tanggal Kembali" name="tgl_kembali">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                        <div class="invalid-feedback" id="tgl_kembali"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ';
                    $modal_footer = '<button type="submit" class="btn btn-primary" data-href="/admin/trx-pengembalian" data-tbl="#tabel_transaksi_peminjaman" data-mtd="POST"><i class="fas fa-save"></i> Simpan</button>
                                     <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-window-close"></i> Tutup</button>';
    
                    $data = array('modal_size' => $modal_size,'modal_header' => $modal_header, 'modal_body' => $modal_body, 'modal_footer' => $modal_footer);
                }
                
                return response()->json($data);
            break;
            
            default:
                # code...
            break;
        }
    }
}
