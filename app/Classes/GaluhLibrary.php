<?php
namespace App\Classes;
use DB;
use Crypt;
use Carbon\Carbon;
use App\User;
use App\Models\AnggotaModel;

class GaluhLibrary {

    public static function dropdownKategori($selected){
        $kategoris = DB::select("SELECT id_kategori,nama_kat FROM tb_kategori WHERE deleted_at IS NULL ORDER BY nama_kat ASC");
        $options   = [];
        foreach ($kategoris as $key => $kategori) {
            $options[] = '<option value="'.$kategori->id_kategori.'" '.($selected == $kategori->id_kategori ? "selected" : " ").'>'.$kategori->nama_kat.'</option>';
        }

        return implode('', $options);
    }

    public static function dropdownLokasi($selected){
        $lokasi = DB::select("SELECT id_lokasi,kode_lokasi,loker FROM tb_lokasi WHERE deleted_at IS NULL ORDER BY kode_lokasi ASC");
        $options   = [];
        foreach ($lokasi as $key => $value) {
            $options[] = '<option value="'.$value->id_lokasi.'" '.($selected == $value->id_lokasi ? "selected" : " ").'>'.$value->kode_lokasi.' | '.$value->loker.'</option>';
        }

        return implode('', $options);
    }

    public static function dropdownAnggota($selected){
        $angggota = DB::select("SELECT id_anggota,nama,pekerjaan,no_hp FROM tb_anggota WHERE deleted_at IS NULL ORDER BY nama ASC");
        $options   = [];
        foreach ($angggota as $key => $value) {
            $options[] = '<option value="'.$value->id_anggota.'" '.($selected == $value->id_anggota ? "selected" : " ").'>'.$value->nama.' | '.$value->no_hp.'</option>';
        }

        return implode('', $options);
    }

    public static function getDataBuku($id)
    {
        $buku = DB::select("SELECT buku.id_buku,buku.judul,buku.pengarang,buku.cover,buku.kuantitas, kat.nama_kat,kat.id_kategori, lok.kode_lokasi,lok.loker,lok.id_lokasi
        FROM tb_buku buku
        LEFT JOIN tb_kategori kat ON kat.id_kategori = buku.id_kategori
        LEFT JOIN tb_lokasi lok ON lok.id_lokasi = buku.id_lokasi
        WHERE buku.deleted_at IS NULL");

        $listBuku = '';
        foreach ($buku as $key => $value) {
            $cover = asset('storage/cover-book/'.$value->cover);
            $listBuku .= '
                <div class="col-md-4">
                    <div class="separator separator-dashed"></div>
                    <div class="d-flex flex-wrap align-items-center mb-5 mt-5">
                        <div class="symbol mr-5">
                            <img class="symbol-label min-w-65px min-h-100px mr-4" src="'.$cover.'">
                        </div>
                        <div class="d-flex flex-column">
                            <a href="javascript:;" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">'.$value->judul.'</a>
                            <span class="text-muted font-weight-bold font-size-sm pb-4">PENGARANG :
                            <br>'.$value->pengarang.'</span>
                            <div>
                                <div class="checkbox-inline">
                                    <label class="checkbox checkbox-lg">
                                    <input type="checkbox" value="'.$value->id_buku.'" name="id_buku[]">
                                    <span></span>Pilih Buku</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="separator separator-dashed"></div>
                </div>
            ';
        }

        return $listBuku;
    }

    public static function getDataBukuView($id)
    {
        $data = DB::select("SELECT pinjam.id_pinjam,pinjam.id_transaksi,pinjam.id_buku,pinjam.tgl_pinjam,pinjam.status,
        buku.judul,buku.pengarang,buku.cover
        FROM tb_peminjaman pinjam
        LEFT JOIN tb_buku buku ON buku.id_buku = pinjam.id_buku
        WHERE pinjam.deleted_at IS NULL AND pinjam.id_transaksi = $id");
        
        $listBuku = '';
        foreach ($data as $key => $value) {
            $cover = asset('storage/cover-book/'.$value->cover);
            $listBuku .= '
                <div class="col-md-4">
                    <div class="separator separator-dashed"></div>
                    <div class="d-flex flex-wrap align-items-center mb-5 mt-5">
                        <div class="symbol mr-5">
                            <img class="symbol-label min-w-65px min-h-100px mr-4" src="'.$cover.'">
                        </div>
                        <div class="d-flex flex-column">
                            <a href="javascript:;" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">'.$value->judul.'</a>
                            <span class="text-muted font-weight-bold font-size-sm pb-4">PENGARANG :
                            <br>'.$value->pengarang.'</span>
                            <div>
                                <span class="label label-lg label-light-'.($value->status == 'Dipinjam.' ? 'danger' : 'success').' label-inline">'.$value->status.'</span>
                            </div>
                        </div>
                    </div>
                    <div class="separator separator-dashed"></div>
                </div>
            ';
        }

        return $listBuku;
    }

    public static function getNamaAnggota($id)
    {
        $namaAnggota = AnggotaModel::find($id);
        return $namaAnggota->nama.' - '.$namaAnggota->no_hp;
    }

    public static function getPekerjaanAnggota($id)
    {
        $namaAnggota = AnggotaModel::find($id);
        return $namaAnggota->pekerjaan;
    }
}