<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnggotaModel extends Model
{
    use SoftDeletes;

    protected $table = 'tb_anggota';
    protected $primaryKey = 'id_anggota';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'nama', 'pekerjaan', 'no_hp',
    ];
}
