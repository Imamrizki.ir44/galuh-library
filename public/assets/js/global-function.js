$("#form").submit(function(e) {
    e.preventDefault();
    var data = $("#form");
    var url  = $("#form :button:submit").data("href");
    var mtd  = $("#form :button:submit").data("mtd");
    var tbl  = $("#form :button:submit").data("tbl");

    var typePost = data.find("input[type='file']")[0];
    if (typePost != null) {
        var form = $(this)[0];
        data = new FormData(form);
        ajaxSimpanDataWithFile(url, mtd, tbl, data);
    }else{
        ajaxSimpanData(url, mtd, tbl, data);
    }
});

// ajax function simpan data
function ajaxSimpanData(url, mtd, tbl, data) {  
    setTimeout(function(){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: mtd,
            url: url,
            data: data.serialize(),
            success: function(response) {
                $(tbl).DataTable().ajax.reload(null, false);
                $("#myModal").modal("hide");
                $("#form").trigger("reset");
                showSuccessToast(response.message);
            },
            error: function(response) {
                $.each(response.responseJSON.errors, function(name,value){
                    $('[name='+name+']').addClass('is-invalid');
                    $('#'+name+'').text(value[0]);
                    
                    $('input,textarea,select').on('keydown keypress keyup click change',function(){
                        $(this).removeClass('is-invalid'); 
                        $(this).closest( "div.form-group" ).removeClass('is-invalid');
                        $(this).closest( "div" ).removeClass('kt-option');                                                                                                            
                        $(this).nextAll('.text-danger').hide();
                    });
                });
            }
        });
    }, 200);
}

function ajaxSimpanDataWithFile(url, mtd, tbl, data) {  
    setTimeout(function(){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });
        $.ajax({
            type: mtd,
            url: url,
            data: data,
            cache : false,
            contentType : false,
            processData : false,
            success: function(response) {
                $(tbl).DataTable().ajax.reload(null, false);
                $("#myModal").modal("hide");
                $("#form").trigger("reset");
                showSuccessToast(response.message);
            },
            error: function(response) {
                $.each(response.responseJSON.errors, function(name,value){
                    $('[name='+name+']').addClass('is-invalid');
                    $('#'+name+'').text(value[0]);
                    
                    $('input,textarea,select').on('keydown keypress keyup click change',function(){
                        $(this).removeClass('is-invalid'); 
                        $(this).closest( "div.form-group" ).removeClass('is-invalid');
                        $(this).closest( "div" ).removeClass('kt-option');                                                                                                            
                        $(this).nextAll('.text-danger').hide();
                    });
                });
            }
        });
    }, 200);
}

$(document).on("click", ".hapusData", function() {
    var url  = $(this).data("href");
    var id   = $(this).data("id");
    var tbl  = $(this).data("tbl");
    ajaxDelete(url, id, tbl);
});

function ajaxDelete(url, id, tbl) {
    Swal.fire({
        title: "Apakah Anda yakin?",
        text: "Data yang Anda hapus tidak dapat dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak"
    }).then(result => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content")
                }
            });
            $.ajax({
                url: url,
                type: "DELETE",
                data: { id: id },
                success: function(response) {
                    $(tbl).DataTable().ajax.reload(null, false);
                    showSuccessToast(response.message);
                }
            });
        }
    });
}