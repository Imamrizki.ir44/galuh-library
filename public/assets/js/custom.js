// toast
(function ($) {
    showSuccessToast = function (msg) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Success',
            text: msg,
            showHideTransition: 'slide',
            icon: 'success',
            loaderBg: '#f96868',
            position: 'top-right',
            hideAfter: 5000
        })
    };
    showInfoToast = function (msg) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Info',
            text: msg,
            showHideTransition: 'slide',
            icon: 'info',
            loaderBg: '#46c35f',
            position: 'top-right',
            hideAfter: 5000
        })
    };
    showWarningToast = function (msg) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Warning',
            text: msg,
            showHideTransition: 'slide',
            icon: 'warning',
            loaderBg: '#57c7d4',
            position: 'top-right',
            hideAfter: 9000
        })
    };
    showDangerToast = function (msg) {
        'use strict';
        resetToastPosition();
        $.toast({
            heading: 'Danger',
            text: msg,
            showHideTransition: 'slide',
            icon: 'error',
            loaderBg: '#f2a654',
            position: 'top-right',
            hideAfter: 15000
        })
    };
    resetToastPosition = function () {
        $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
        $(".jq-toast-wrap").css({
            "top": "",
            "left": "",
            "bottom": "",
            "right": ""
        }); //to remove previous position style
    }
})(jQuery);

function removeClassModal() {
    $("#myModal .modal-dialog").removeClass("modal-full");
    $("#myModal .modal-dialog").removeClass("modal-lg");
    $("#myModal .modal-dialog").removeClass("modal-xl");
    $("#myModal .modal-dialog").removeClass("modal-sm");
  }