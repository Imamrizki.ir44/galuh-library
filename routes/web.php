<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function(){
    return redirect(route('dashboard'));
});

Route::prefix('admin')->group(function() {
    Route::post('/get-all-modal', 'ModalController@componentModal')->name('get_all_modal');

    Route::get('/dashboard', 'DashboardController@page')->name('dashboard');
    Route::get('/data-pinjam', 'DashboardController@dataPinjam')->name('data_pinjam');

    // Route Anggota
    Route::get('/anggota', 'AnggotaController@page')->name('anggota');
    Route::get('/get-tabel-anggota', 'AnggotaController@tabelAnggota')->name('get_table_anggota');
    Route::match(['post', 'delete', 'put'], '/crud-anggota/{id?}', 'AnggotaController@crudAnggota');

    // Route Buku
    Route::get('/buku', 'BukuController@page')->name('buku');
    Route::get('/get-tabel-buku', 'BukuController@tabelBuku')->name('get_table_buku');
    Route::match(['post', 'delete', 'put'], '/crud-buku/{id?}', 'BukuController@crudBuku');

    // Route Kategori
    Route::get('/get-tabel-kategori', 'BukuController@tabelKategori')->name('get_table_kategori');
    Route::match(['post', 'delete', 'put'], '/crud-kategori/{id?}', 'BukuController@crudKategori');

    // Route Lokasi
    Route::get('/get-tabel-lokasi', 'BukuController@tabelLokasi')->name('get_table_lokasi');
    Route::match(['post', 'delete', 'put'], '/crud-lokasi/{id?}', 'BukuController@crudLokasi');
    
    // Route Peminjaman
    Route::get('/peminjaman', 'PeminjamanController@page')->name('peminjaman');
    Route::get('/get-tabel-trx', 'PeminjamanController@getDataPeminjaman')->name('get_table_trx');
    Route::match(['post', 'delete', 'put'], '/crud-trx-peminjaman/{id?}', 'PeminjamanController@crudPeminjaman');
    Route::post('/trx-pengembalian', 'PeminjamanController@pengembalian');
    Route::post('/pencarian-buku', 'PeminjamanController@pencarianBuku')->name('pencarian_buku');

    // Route Riwayat
    Route::get('/riwayat', 'RiwayatController@page')->name('riwayat');
});
