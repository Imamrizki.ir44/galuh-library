<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\AnggotaModel::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'pekerjaan' => $faker->jobTitle(),
        'no_hp' => $faker->e164PhoneNumber()
    ];
});
