<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('DELETE FROM tb_kategori');

        DB::transaction(function () {
            $kategoris = array(
                "Novel", 
                "Cergam", 
                "Komik", 
                "Ensiklopedia", 
                "Nomik", 
                "Antologi", 
                "Dongeng", 
                "Biografi", 
                "Novelet", 
                "Fotografi", 
                "Karya ilmiah", 
                "Tafsir", 
                "Kamus", 
                "Panduan (how to)", 
                "Atlas", 
                "Ilmiah", 
                "Teks", 
                "Mewarnai", 
                "Catatan harian (jurnal/diary)"
            );

            $now = Carbon::now();
            foreach ($kategoris as $key => $kategori) {
                $id = $key+1;
                DB::statement("INSERT INTO tb_kategori(id_kategori,nama_kat,created_at,updated_at) VALUES ($id,'$kategori','$now','$now')");
            }
        });
    }
}
