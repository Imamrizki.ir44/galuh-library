<div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" id="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <form role="form" id="form" enctype="multipart/form-data" name="myForm">
                <div class="modal-body" id="modal-body">
                </div>
                <div class="modal-footer" id="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>