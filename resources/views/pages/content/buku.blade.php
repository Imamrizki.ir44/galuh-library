@extends('pages.layout.template')
@section('title','Buku')
@section('data-master-active','menu-item-open menu-item-here')
@section('sub-buku-active','menu-item-active')
@push('style')
   <link rel="stylesheet" href="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom gutter-b">
           <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Tabel Buku
                 </h3>
            </div>
            <div class="card-toolbar">
                <a href="javascript:;" class="btn btn-primary buttonBuku" data-name="modalBuku" data-id="0"><i class="flaticon2-add"></i>Tambah Buku</a>
            </div>
           </div>
           <div class="card-body">
              <table class="table table-separate table-head-custom table-checkable" id="tabel_buku">
                 <thead>
                    <tr>
                       <th style="width: 5%">No</th>
                       <th style="width: 5%">Cover</th>
                       <th style="width: 25%">Judul</th>
                       <th style="width: 15%">Kategori</th>
                       <th style="width: 20%">Pengarang</th>
                       <th style="width: 15%">Kode Lokasi - Loker</th>
                       <th style="width: 15%">Actions</th>
                    </tr>
                 </thead>
        
                 <tbody>
                    
                 </tbody>
              </table>
           </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card card-custom gutter-b">
           <div class="card-header">
              <div class="card-title">
                    <h3 class="card-label">
                       Tabel Kategori
                    </h3>
              </div>
              <div class="card-toolbar">
                 <a href="javascript:;" class="btn btn-primary buttonKategori" data-name="modalKategori" data-id="0"><i class="flaticon2-add"></i>Tambah Kategori</a>
              </div>
           </div>
           <div class="card-body">
              <table class="table table-separate table-head-custom table-checkable" id="tabel_kategori">
                 <thead>
                    <tr>
                       <th>No</th>
                       <th>Kategori</th>
                       <th>Actions</th>
                    </tr>
                 </thead>
        
                 <tbody>
                    
                 </tbody>
              </table>
           </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-custom gutter-b">
           <div class="card-header">
              <div class="card-title">
                    <h3 class="card-label">
                       Tabel Lokasi
                    </h3>
              </div>
              <div class="card-toolbar">
                 <a href="javascript:;" class="btn btn-primary buttonLokasi" data-name="modalLokasi" data-id="0"><i class="flaticon2-add"></i>Tambah Lokasi</a>
              </div>
           </div>
           <div class="card-body">
              <table class="table table-separate table-head-custom table-checkable" id="tabel_lokasi">
                 <thead>
                    <tr>
                       <th>No</th>
                       <th>Kode Lokasi</th>
                       <th>Loker</th>
                       <th>Actions</th>
                    </tr>
                 </thead>
        
                 <tbody>
                    
                 </tbody>
              </table>
           </div>
        </div>
    </div>
</div>
@endsection
@push('script')
   <script src="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
   <script>
      $(document).ready(function () {
          // get datatable yajra buku
          $(function () {
              var tabel_buku = $("#tabel_buku").DataTable({
                  processing: true,
                  serverSide: true,
                  ordering: false,
                  responsive: true,
                  ajax: "{{ route('get_table_buku') }}",
                  columns: [{
                          data: 'DT_RowIndex',
                          name: 'DT_RowIndex',
                          class: "text-center"
                      },
                      {
                          data: 'cover',
                          name: 'cover',
                          class: "text-center"
                      },
                      {
                          data: 'judul',
                          name: 'judul'
                      },
                      {
                          data: 'nama_kat',
                          name: 'nama_kat'
                      },
                      {
                          data: 'pengarang',
                          name: 'pengarang'
                      },
                      {
                          data: 'lokasi',
                          name: 'lokasi'
                      },
                      {
                          data: 'action',
                          name: 'action',
                          class: "text-center"
                      }
                  ]
              })
          })

          // add + edit modal buku
          $(document).on('click', '.buttonBuku', function () {
              var dataName = $(this).data('name');
              var dataId = $(this).data('id');

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

              $.ajax({
                  url: "{{ route('get_all_modal') }}",
                  type: 'POST',
                  data: {
                      dataId: dataId,
                      dataName: dataName
                  },
                  success: function (response) {
                    removeClassModal();
                    $('#modal-dialog').addClass(response.modal_size);
                    $('#modal-title').html(response.modal_header);
                    $('#modal-body').html(response.modal_body);
                    $('#modal-footer').html(response.modal_footer);
                    $('#myModal').modal({
                        'backdrop': 'static'
                    });
                    $('#myModal').modal('show');

                    // preview name file
                    if (response.file_cover !== null && response.file_cover !== '') {
                        $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(response.file_cover);
                    }

                  }
              })
          })

          // get datatable yajra kategori
          $(function () {
              var tabel_kategori = $("#tabel_kategori").DataTable({
                  processing: true,
                  serverSide: true,
                  ordering: false,
                  responsive: true,
                  ajax: "{{ route('get_table_kategori') }}",
                  columns: [{
                          data: 'DT_RowIndex',
                          name: 'DT_RowIndex',
                          class: "text-center"
                      },
                      {
                          data: 'nama_kat',
                          name: 'nama_kat'
                      },
                      {
                          data: 'action',
                          name: 'action'
                      }
                  ]
              })
          })

          // add + edit modal kategori
          $(document).on('click', '.buttonKategori', function () {
              var dataName = $(this).data('name');
              var dataId = $(this).data('id');

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

              $.ajax({
                  url: "{{ route('get_all_modal') }}",
                  type: 'POST',
                  data: {
                      dataId: dataId,
                      dataName: dataName
                  },
                  success: function (response) {
                      removeClassModal();
                      $('#modal-dialog').addClass(response.modal_size);
                      $('#modal-title').html(response.modal_header);
                      $('#modal-body').html(response.modal_body);
                      $('#modal-footer').html(response.modal_footer);
                      $('#myModal').modal({
                          'backdrop': 'static'
                      });
                      $('#myModal').modal('show');
                  }
              })
          })

          // get datatable yajra lokasi
          $(function () {
              var tabel_lokasi = $("#tabel_lokasi").DataTable({
                  processing: true,
                  serverSide: true,
                  ordering: false,
                  responsive: true,
                  ajax: "{{ route('get_table_lokasi') }}",
                  columns: [{
                          data: 'DT_RowIndex',
                          name: 'DT_RowIndex',
                          class: "text-center"
                      },
                      {
                          data: 'kode_lokasi',
                          name: 'kode_lokasi'
                      },
                      {
                          data: 'loker',
                          name: 'loker'
                      },
                      {
                          data: 'action',
                          name: 'action'
                      }
                  ]
              })
          })

          // add + edit modal lokasi
          $(document).on('click', '.buttonLokasi', function () {
              var dataName = $(this).data('name');
              var dataId = $(this).data('id');

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

              $.ajax({
                  url: "{{ route('get_all_modal') }}",
                  type: 'POST',
                  data: {
                      dataId: dataId,
                      dataName: dataName
                  },
                  success: function (response) {
                      removeClassModal();
                      $('#modal-dialog').addClass(response.modal_size);
                      $('#modal-title').html(response.modal_header);
                      $('#modal-body').html(response.modal_body);
                      $('#modal-footer').html(response.modal_footer);
                      $('#myModal').modal({
                          'backdrop': 'static'
                      });
                      $('#myModal').modal('show');
                  }
              })
          })

        $('#myModal').on('shown.bs.modal', function() {
            $("[name='kategori']").select2();
            $("[name='lokasi']").select2();

            // preview file name
            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
        })

      });
   </script>
@endpush