@extends('pages.layout.template')
@section('title','Riwayat')
@section('transaksi-active','menu-item-open menu-item-here')
@section('sub-riwayat-active','menu-item-active')
@push('style')
   <link rel="stylesheet" href="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
@endpush
@section('content')
<div class="card card-custom gutter-b">
   <div class="card-header">
      <div class="card-title">
            <h3 class="card-label">
               Tabel Riwayat
            </h3>
      </div>
      <div class="card-toolbar">
         <a href="javascript:;" class="btn btn-primary buttonAnggota" data-name="modalAnggota" data-id="0"><i class="flaticon2-add"></i>Tambah Riwayat</a>
      </div>
   </div>
   <div class="card-body">
      <table class="table table-separate table-head-custom table-checkable" id="tabel_anggota">
         <thead>
            <tr>
               <th>No</th>
               <th>Nama</th>
               <th>Pekerjaan</th>
               <th>Nomor Hp</th>
               <th>Actions</th>
            </tr>
         </thead>

         <tbody>
            
         </tbody>
      </table>
   </div>
</div>
@endsection
@push('script')
   <script src="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
   <script>
      $(document).ready(function(){
         // get datatable yajra anggota
        //  $(function(){
        //     var table = $("#tabel_anggota").DataTable({
        //        processing: true,
        //        serverSide: true,
        //        ordering  : false,
        //        responsive: true,
        //        ajax      : "{{ route('get_table_anggota') }}",
        //        columns   : [
        //           {
        //              data: 'DT_RowIndex',
        //              name: 'DT_RowIndex',
        //              class: "text-center"
        //           },
        //           {
        //              data: 'nama',
        //              name: 'nama'
        //           },
        //           {
        //              data: 'pekerjaan',
        //              name: 'pekerjaan'
        //           },
        //           {
        //              data: 'no_hp',
        //              name: 'no_hp'
        //           },
        //           {
        //              data: 'action',
        //              name: 'action'
        //           }
        //        ]
        //     })
        //  })

         // add + edit modal
         $(document).on('click', '.buttonAnggota', function(){
            var dataName = $(this).data('name');
            var dataId = $(this).data('id');

            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });

            $.ajax({
               url  : '{{ route('get_all_modal') }}',
               type : 'POST',
               data : {
                  dataId : dataId,
                  dataName : dataName
               },
               success : function(response){
                  $('#modal-dialog').addClass(response.modal_size);
                  $('#modal-title').html(response.modal_header);
                  $('#modal-body').html(response.modal_body);
                  $('#modal-footer').html(response.modal_footer);
                  $('#myModal').modal({'backdrop': 'static'});
                  $('#myModal').modal('show');
               }
            })
         })
      });
   </script>
@endpush