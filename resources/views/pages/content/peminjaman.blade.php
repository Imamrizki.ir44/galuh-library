@extends('pages.layout.template')
@section('title','Peminjaman')
@section('transaksi-active','menu-item-open menu-item-here')
@section('sub-peminjaman-active','menu-item-active')
@push('style')
   <link rel="stylesheet" href="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
@endpush
@section('content')
<div class="card card-custom gutter-b">
   <div class="card-header">
      <div class="card-title">
            <h3 class="card-label">
               Tabel Transaksi Peminjaman
            </h3>
      </div>
      <div class="card-toolbar">
         
      </div>
   </div>
   <div class="card-body">
      <table class="table table-separate table-head-custom table-checkable" id="tabel_transaksi_peminjaman">
         <thead>
            <tr>
               <th>No</th>
               <th>Nama Anggota</th>
               <th>Tanggal Transaksi</th>
               <th style="text-align: center">
                  <a href="javascript:;" class="btn btn-primary btn-sm buttonTransaksiPinjam" data-type="addEdit" data-name="modalTransaksiPinjam" data-id="0">
                     <i class="flaticon2-add"></i> Tambah
                  </a>
               </th>
            </tr>
         </thead>

         <tbody>
            
         </tbody>
      </table>
   </div>
</div>
@endsection
@push('script')
   <script src="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
   <script>
      $(document).ready(function(){
         // get datatable yajra anggota
         $(function(){
            var table = $("#tabel_transaksi_peminjaman").DataTable({
               processing: true,
               serverSide: true,
               ordering  : false,
               responsive: true,
               ajax      : "{{ route('get_table_trx') }}",
               columns   : [
                  {
                     data: 'DT_RowIndex',
                     name: 'DT_RowIndex',
                     class: "text-center"
                  },
                  {
                     data: 'anggota',
                     name: 'anggota'
                  },
                  {
                     data: 'tanggal_trx',
                     name: 'tanggal_trx'
                  },
                  {
                     data: 'action',
                     name: 'action',
                     class: "text-center"
                  }
               ]
            })
         })

         // add + edit modal
         $(document).on('click', '.buttonTransaksiPinjam', function(){
            var dataName = $(this).data('name');
            var dataId = $(this).data('id');
            var dataType = $(this).data('type');

            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });

            $.ajax({
               url  : '{{ route('get_all_modal') }}',
               type : 'POST',
               data : {
                  dataId : dataId,
                  dataName : dataName,
                  dataType : dataType
               },
               success : function(response){
                  removeClassModal();
                  $('#modal-dialog').addClass(response.modal_size);
                  $('#modal-title').html(response.modal_header);
                  $('#modal-body').html(response.modal_body);
                  $('#modal-footer').html(response.modal_footer);
                  $('#myModal').modal({'backdrop': 'static'});
                  $('#myModal').modal('show');
               }
            })
         })

         $('#myModal').on('shown.bs.modal', function() {
            $("[name='anggota']").select2();
            $(".date").datepicker({
               todayHighlight: new Date(),
               autoclose: true,
               locale: 'id',
               format: 'dd M yyyy',
            });

            $(document).on('keyup', '#pencarianBuku', function() {
               var cariBuku = $(this).val();

               $.ajax({
                  url      : "{{ route('pencarian_buku') }}",
                  method   : 'POST',
                  dataType : 'json',
                  data     : {
                     '_token' : '{{ csrf_token() }}',
                     cariBuku : cariBuku
                  },
                  success  : function (response) {
                     var itemRow = '';

                     $('#rowSearchResult').html('');

                     $.each(response.data, function(index, value) {
                        itemRow = '<div class="col-md-4">\
                                    <div class="separator separator-dashed"></div>\
                                    <div class="d-flex flex-wrap align-items-center mb-5 mt-5">\
                                          <div class="symbol mr-5">\
                                             <img class="symbol-label min-w-65px min-h-100px mr-4" src="'+response.cover[index]+'">\
                                          </div>\
                                          <div class="d-flex flex-column">\
                                             <a href="javascript:;" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">'+value.judul+'</a>\
                                             <span class="text-muted font-weight-bold font-size-sm pb-4">PENGARANG :\
                                             <br>'+value.pengarang+'</span>\
                                             <div>\
                                                <div class="checkbox-inline">\
                                                      <label class="checkbox checkbox-lg">\
                                                      <input type="checkbox" value="'+value.id_buku+'" name="id_buku[]">\
                                                      <span></span>Pilih Buku</label>\
                                                </div>\
                                             </div>\
                                          </div>\
                                    </div>\
                                    <div class="separator separator-dashed"></div>\
                                 </div>';

                        $('#rowSearchResult').append(itemRow);
                     });
                  }
               })
            })
        })
      });
   </script>
@endpush