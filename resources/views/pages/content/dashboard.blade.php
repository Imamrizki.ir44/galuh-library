@extends('pages.layout.template')
@section('title','Dashboard')
@section('dashboard-active','menu-item-open menu-item-here')
@push('style')
    
@endpush
@section('content')
<div class="row">
   <div class="col-md-8">
      <div class="card card-custom card-stretch gutter-b">
         <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
               <span class="card-label font-weight-bolder text-dark">Table & Grafik Peminjaman</span>
            </h3>
         </div>
         <div class="card-body py-0">
            <div class="table-responsive">
               <table class="table table-head-custom table-vertical-center" id="kt_advance_table_widget_1">
                  <thead>
                     <tr class="text-left">
                        <th style="min-width: 10px"></th>
                        <th class="pr-0" style="width: 500px">anggota - nomor hp</th>
                        <th style="min-width: 270px">Tanggal Jatuh Tempo</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if (count($data) != 0)
                        @foreach ($data as $item)
                           <tr>
                              <td>
                                 <div class="symbol symbol-50 symbol-light mt-1">
                                    <span class="symbol-label">
                                       <img src="{{ URL::asset('assets/media/users/038-boy-16.svg') }}" class="h-75 align-self-end" style="width: 100%; height: 100%" alt="">
                                    </span>
                                 </div>
                              </td>
                              <td>
                                 <a href="javascript:;" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg buttonTransaksiPinjam" data-name="modalTransaksiPinjam" data-type="lihat" data-id="{{$item->id_transaksi}}" title="Lihat Peminjaman">
                                    {{GaluhLibrary::getNamaAnggota($item->id_anggota)}}
                                 </a>
                                 <span class="text-muted font-weight-bold text-muted d-block">{{GaluhLibrary::getPekerjaanAnggota($item->id_anggota)}}</span>
                              </td>
                              <td>
                                 @php
                                    $jatuhTempo = \Carbon\Carbon::parse($item->tgl_jatuh_tempo);
                                    $now = \Carbon\Carbon::now();
                                    if ($jatuhTempo->diffInDays($now) <= 2) {
                                       $color = 'danger';
                                    }else{
                                       $color = 'success';
                                    }
                                 @endphp
                                 <span class="label label-lg label-light-{{$color}} label-inline">{{\Carbon\Carbon::parse($item->tgl_jatuh_tempo)->format('d M Y')}}</span>
                              </td>
                           </tr>
                        @endforeach
                     @else
                        <tr>
                           <td></td>
                           <td><label for="">Tidak ada data peminjaman pada table ini</label></td>
                           <td></td>
                        </tr>
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-4">
      <div class="card card-custom gutter-b">
         <div class="card-body py-0">
            <div id="container"></div>
         </div>
      </div>
   </div>
</div>
@endsection
@push('script')
   <script src="https://code.highcharts.com/highcharts.js"></script>
   <script>
      $(document).ready(function(){

         $.getJSON("{{route('data_pinjam')}}", function(response){
            if (response.status == 200) {
               Highcharts.chart('container', {
                   chart: {
                       plotBackgroundColor: null,
                       plotBorderWidth: null,
                       plotShadow: false,
                       type: 'pie'
                   },
                   title: {
                       text: ''
                   },
                   tooltip: {
                       pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                   },
                   accessibility: {
                       point: {
                           valueSuffix: '%'
                       }
                   },
                   credits: {
                       enabled: false
                   },
                   plotOptions: {
                       pie: {
                           allowPointSelect: true,
                           cursor: 'pointer',
                           dataLabels: {
                               enabled: false
                           },
                           showInLegend: true
                       }
                   },
                   series: [{
                       name: 'Total',
                       colorByPoint: true,
                       data: [{
                           name: 'Dipinjam',
                           y: response.data[1].pinjam,
                           color: "#f64e60"
                       }, {
                           name: 'Dikembalikan',
                           y: response.data[0].kembali,
                           color: "#1bc5bd"
                       }]
                   }]
               });
            }
         });

         $(document).on('click', '.buttonTransaksiPinjam', function(){
            var dataName = $(this).data('name');
            var dataId = $(this).data('id');
            var dataType = $(this).data('type');

            $.ajaxSetup({
               headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            });

            $.ajax({
               url  : '{{ route('get_all_modal') }}',
               type : 'POST',
               data : {
                  dataId : dataId,
                  dataName : dataName,
                  dataType : dataType
               },
               success : function(response){
                  removeClassModal();
                  $('#modal-dialog').addClass(response.modal_size);
                  $('#modal-title').html(response.modal_header);
                  $('#modal-body').html(response.modal_body);
                  $('#modal-footer').html(response.modal_footer);
                  $('#myModal').modal({'backdrop': 'static'});
                  $('#myModal').modal('show');
               }
            })
         })

      });
   </script>
@endpush