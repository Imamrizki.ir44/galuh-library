<div class="header-top">
    <div class=" container-fluid ">
        <div class="d-none d-lg-flex align-items-center mr-3">
            <a href="index.html" class="mr-20">
            <img alt="Logo" src="{{ URL::asset('assets/media/logos/logo-galuh-lib.png') }}" class="max-h-45px"/>
            </a>
        </div>
        <div class="topbar">
            <div class="topbar-item">
            <div class="btn btn-icon btn-hover-transparent-white w-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                <div class="d-flex flex-column text-right pr-3">
                    <span class="text-white opacity-50 font-weight-bold font-size-sm d-none d-md-inline">Admin</span>
                    <span class="text-white font-weight-bolder font-size-sm d-none d-md-inline">Administrator</span>
                </div>
                <span class="symbol symbol-35">
                <span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-30">A</span>
                </span>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="header-bottom">
    <div class=" container-fluid ">
        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
            <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default " >
            <ul class="menu-nav ">
                <li class="menu-item @yield('dashboard-active')">
                    <a  href="{{ route('dashboard') }}" class="menu-link"><span class="menu-text">Dashboard</span><span class="menu-desc">Informasi Grafik Peminjaman</span></a>
                </li>
                <li class="menu-item @yield('data-master-active') menu-item-submenu menu-item-rel"  data-menu-toggle="hover" aria-haspopup="true">
                    <a  href="javascript:;" class="menu-link menu-toggle"><span class="menu-text">Data Master</span><span class="menu-desc">Kebutuhan Master Data</span><i class="menu-arrow"></i></a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                        <ul class="menu-subnav">
                        {{-- <li class="menu-item "  aria-haspopup="true"><a  href="javascript:;" class="menu-link "><span class="menu-text">Petugas</span><span class="menu-desc"></span></a></li> --}}
                        <li class="menu-item @yield('sub-anggota-active')"  aria-haspopup="true"><a href="{{ route('anggota') }}" class="menu-link "><span class="menu-text">Anggota</span><span class="menu-desc"></span></a></li>
                        <li class="menu-item @yield('sub-buku-active')"  aria-haspopup="true"><a href="{{ route('buku') }}" class="menu-link "><span class="menu-text">Buku</span><span class="menu-desc"></span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item @yield('transaksi-active')">
                    <a  href="{{ route('peminjaman') }}" class="menu-link"><span class="menu-text">Transaksi</span><span class="menu-desc">Rekap Data Transaksi</span></a>
                </li>
                {{-- <li class="menu-item @yield('transaksi-active') menu-item-submenu menu-item-rel"  data-menu-toggle="hover" aria-haspopup="true">
                    <a  href="javascript:;" class="menu-link menu-toggle"><span class="menu-text">Transaksi</span><span class="menu-desc">Rekap Data Transaksi</span><i class="menu-arrow"></i></a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                        <ul class="menu-subnav">
                        <li class="menu-item @yield('sub-peminjaman-active')"  aria-haspopup="true"><a  href="{{ route('peminjaman') }}" class="menu-link "><span class="menu-text">Peminjaman</span><span class="menu-desc"></span></a></li>
                        <li class="menu-item @yield('sub-pengembalian-active')"  aria-haspopup="true"><a href="{{ route('pengembalian') }}" class="menu-link "><span class="menu-text">Pengembalian</span><span class="menu-desc"></span></a></li>
                        <li class="menu-item @yield('sub-riwayat-active')"  aria-haspopup="true"><a href="{{ route('riwayat') }}" class="menu-link "><span class="menu-text">Riwayat</span><span class="menu-desc"></span></a></li>
                        </ul>
                    </div>
                </li> --}}
            </ul>
            </div>
        </div>
    </div>
</div>

<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
    <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
       <h3 class="font-weight-bold m-0">
          User Profile
       </h3>
       <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
       <i class="ki ki-close icon-xs text-muted"></i>
       </a>
    </div>
    <div class="offcanvas-content pr-5 mr-n5">
       <div class="d-flex align-items-center mt-5">
          <div class="symbol symbol-100 mr-5">
             <div class="symbol-label" style="background-image:url({{ URL::asset('assets/media/users/default.jpg') }}"></div>
             <i class="symbol-badge bg-success"></i>
          </div>
          <div class="d-flex flex-column">
             <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">
             Admin
             </a>
             <div class="text-muted mt-1">
                Administrator
             </div>
             <div class="navi mt-2">
                <a href="#" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
             </div>
          </div>
       </div>
    </div>
 </div>