{{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/> --}}
<link href="{{ URL::asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/plugins/custom/jquery-toast-plugin/jquery.toast.min.css') }}" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="{{ URL::asset('assets/media/logos/favicon.png') }}"/>