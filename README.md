## Installasi
1. Jalankan perintah <code>"composer install"</code> tanpa tanda "
2. Sesuaikan database .env file
3. Import file sql "galuh-library.sql" yang ada pada project
4. Jalankan perintah <code>"php artisan storage:link"</code> tanpa tanda "
5. Lalu coba run project dengan menjalankan perintah <code>"php artisan serve"</code> tanpa tanda "
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
