-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table galuh-library.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table galuh-library.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.migrations: ~9 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2020_12_04_022922_create_tb_anggota_table', 2),
	(5, '2020_12_04_122657_create_tb_kategori_table', 3),
	(7, '2020_12_04_134702_create_tb_lokasi_table', 4),
	(9, '2020_12_04_143803_create_tb_buku_table', 5),
	(10, '2020_12_05_054551_create_tb_transaksi_table', 6),
	(11, '2020_12_05_054627_create_tb_peminjaman_table', 6),
	(12, '2020_12_05_054656_create_tb_pengembalian_table', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_anggota
CREATE TABLE IF NOT EXISTS `tb_anggota` (
  `id_anggota` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_anggota`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_anggota: ~55 rows (approximately)
DELETE FROM `tb_anggota`;
/*!40000 ALTER TABLE `tb_anggota` DISABLE KEYS */;
INSERT INTO `tb_anggota` (`id_anggota`, `nama`, `pekerjaan`, `no_hp`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Prof. Bette Feil', 'Quis cum vel.', '+1-954-358-9710', NULL, '2020-12-04 03:13:50', '2020-12-04 03:13:50'),
	(2, 'Prof. Nels Luettgen IV', 'Et beatae voluptatem.', '761.809.0193', NULL, '2020-12-04 03:19:26', '2020-12-04 03:19:26'),
	(3, 'Yessenia Green', 'Expedita magnam qui tempora.', '(384) 935-5053 x0766', NULL, '2020-12-04 03:25:07', '2020-12-04 03:25:07'),
	(4, 'Dylan Kiehn DDS', 'Harum debitis ducimus.', '+15465485167', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(5, 'Alva Kilback', 'Porro ab.', '947.231.3000', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(6, 'Warren Johns', 'Quo sed nisi vel.', '697-568-4324', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(7, 'Bethel Mraz', 'Ea eligendi expedita.', '534-682-0602 x0919', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(8, 'Bradford Skiles DDS', 'Qui rerum minus.', '1-942-830-1343', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(9, 'Jacquelyn Beatty', 'Rerum sunt.', '1-893-253-7149', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(10, 'Sarai Gleichner', 'Aut est quisquam.', '772-838-3239', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(11, 'Nick Mueller', 'Suscipit magni minima.', '871.242.4141 x032', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(12, 'Dr. Jordan Hilpert DVM', 'Eligendi voluptate nam.', '1-694-330-7587', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(13, 'Eda Leffler', 'Ipsum et qui aut.', '780.258.5833 x1582', NULL, '2020-12-04 03:25:20', '2020-12-04 03:25:20'),
	(14, 'Casper Gerhold', 'Pariatur quis nemo.', '542-470-6639 x864', NULL, '2020-12-04 03:25:21', '2020-12-04 03:25:21'),
	(15, 'Yoshiko Brakus', 'Voluptatem tenetur nihil sed.', '1-772-748-1531', NULL, '2020-12-04 03:25:21', '2020-12-04 03:25:21'),
	(16, 'Raleigh Raynor', 'Est laborum velit omnis et.', '+1 (942) 898-5516', NULL, '2020-12-04 03:25:21', '2020-12-04 03:25:21'),
	(17, 'Chanelle Wilkinson', 'Expedita corporis a eum.', '+1-681-675-7807', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(18, 'Callie Windler IV', 'Nobis ut et minima.', '(689) 676-6133 x4639', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(19, 'Prof. Benedict O\'Connell MD', 'Repellendus molestias et.', '+1-275-403-7019', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(20, 'Mackenzie Toy', 'Rem eum reiciendis qui.', '+1 (721) 697-4050', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(21, 'Arturo Yundt', 'Quam maxime officia laboriosam.', '1-762-277-1898 x380', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(22, 'Shirley Lehner', 'Numquam ut impedit reiciendis.', '447-700-7738', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(23, 'Braden Daugherty', 'Corrupti qui cupiditate.', '960.702.7249', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(24, 'Devonte Fadel MD', 'Autem quia qui porro.', '(462) 268-9939 x6879', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(25, 'Leilani Von', 'Ullam cum earum.', '897-485-2918', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(26, 'Sam Smitham Jr.', 'Maiores odit eum rerum.', '904.262.8456 x787', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(27, 'Garry Luettgen', 'Similique ex ut.', '(916) 442-4486', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(28, 'Prof. Nathen Gottlieb I', 'Dolor id et.', '294-678-2401 x478', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(29, 'Gregorio DuBuque', 'Quos alias tempore quidem.', '1-840-557-2066 x323', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(30, 'Malvina Bruen', 'Et error vero est.', '543.246.9767', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(31, 'Miss Leda Deckow V', 'Odit voluptates impedit.', '418.660.0414', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(32, 'Grant Robel V', 'Neque vero aut expedita assumenda.', '1-508-685-8790', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(33, 'Arden Grimes III', 'Sapiente qui et.', '764.928.9907 x11278', NULL, '2020-12-04 03:25:38', '2020-12-04 03:25:38'),
	(34, 'Myron Williamson', 'Incidunt adipisci at praesentium.', '1-306-361-4769', NULL, '2020-12-04 03:25:39', '2020-12-04 03:25:39'),
	(35, 'Mr. Jedediah Veum', 'Atque qui.', '1-660-573-8827', NULL, '2020-12-04 03:25:39', '2020-12-04 03:25:39'),
	(36, 'Mr. Elijah Reilly', 'Optio eos non.', '557-852-0032 x31927', NULL, '2020-12-04 03:25:39', '2020-12-04 03:25:39'),
	(37, 'Prof. Alize Sipes', 'Explicabo voluptas possimus.', '804.886.3597 x17948', NULL, '2020-12-04 03:25:50', '2020-12-04 03:25:50'),
	(38, 'Mrs. Sally Feil', 'Quia illo sequi.', '213-598-0710 x42541', NULL, '2020-12-04 03:25:50', '2020-12-04 03:25:50'),
	(39, 'Ozella Ullrich MD', 'Modi ut adipisci officiis ipsum.', '+1-792-893-6959', NULL, '2020-12-04 03:25:50', '2020-12-04 03:25:50'),
	(40, 'Trevion McClure Jr.', 'Excepturi labore eaque quis.', '+15759888971', NULL, '2020-12-04 03:25:54', '2020-12-04 03:25:54'),
	(41, 'Mr. Fernando Gibson III', 'Iste libero optio.', '1-321-888-0019', NULL, '2020-12-04 03:25:54', '2020-12-04 03:25:54'),
	(42, 'Miller Stanton', 'Placeat suscipit voluptatum natus.', '+1-643-787-5995', NULL, '2020-12-04 03:25:58', '2020-12-04 03:25:58'),
	(43, 'Rod Dickinson', 'Ad earum earum quasi aut.', '627-618-6164', NULL, '2020-12-04 03:25:58', '2020-12-04 03:25:58'),
	(44, 'Greg Stokes', 'Ratione facilis.', '(556) 548-6119 x663', NULL, '2020-12-04 03:25:58', '2020-12-04 03:25:58'),
	(45, 'Mitchell Corwin', 'Consequatur natus.', '402.702.6165 x07356', NULL, '2020-12-04 03:27:14', '2020-12-04 09:57:28'),
	(46, 'as', 'as', 'as', '2020-12-04 09:51:36', '2020-12-04 08:34:56', '2020-12-04 09:51:36'),
	(47, 'imam', 'programmer', '08131233', '2020-12-04 09:56:29', '2020-12-04 08:40:53', '2020-12-04 09:56:29'),
	(48, 'icim', 'isdkdad', 'iasdas', '2020-12-04 09:46:51', '2020-12-04 08:45:13', '2020-12-04 09:46:51'),
	(49, 'sdfs', 'ssdd', '343', '2020-12-04 09:46:22', '2020-12-04 08:49:50', '2020-12-04 09:46:22'),
	(50, 'sdgdd', 'sdsddd', '34534', '2020-12-04 09:44:22', '2020-12-04 08:50:34', '2020-12-04 09:44:22'),
	(51, 'imam', 'designer', '133', '2020-12-04 09:35:18', '2020-12-04 09:10:15', '2020-12-04 09:35:18'),
	(52, 'Imam Rizki Julian', 'Programmer', '089', '2020-12-04 10:34:39', '2020-12-04 09:58:13', '2020-12-04 10:34:39'),
	(53, 'tes', 'tea', '33', '2020-12-05 20:06:23', '2020-12-04 13:14:46', '2020-12-05 20:06:23'),
	(54, 's', 'sd', '34', '2020-12-05 20:06:26', '2020-12-04 17:35:25', '2020-12-05 20:06:26'),
	(55, 'asd', 'asd', '34', '2020-12-05 20:06:29', '2020-12-04 17:42:08', '2020-12-05 20:06:29');
/*!40000 ALTER TABLE `tb_anggota` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_buku
CREATE TABLE IF NOT EXISTS `tb_buku` (
  `id_buku` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `judul` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengarang` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kuantitas` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_buku`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_buku: ~6 rows (approximately)
DELETE FROM `tb_buku`;
/*!40000 ALTER TABLE `tb_buku` DISABLE KEYS */;
INSERT INTO `tb_buku` (`id_buku`, `id_kategori`, `id_lokasi`, `judul`, `pengarang`, `cover`, `kuantitas`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 6, 1, 'MY LIFE AS AN ICE CREAM SANDWICH', 'IBI ZOBOI', '1607140682_MY LIFE AS AN ICE CREAM SANDWICH.png', 5, NULL, '2020-12-05 03:58:02', NULL),
	(2, 17, 3, 'THE MEMO', 'MINDA HARTS', '1607157788_THE MEMO.png', 5, NULL, '2020-12-05 08:43:08', NULL),
	(3, 8, 3, 'BAKARY ON SAFARI', 'PRATYUSH & RITUPARNA CHARTTER', '1607157880_BAKARY ON SAFARI.png', 3, NULL, '2020-12-05 08:44:40', NULL),
	(4, 4, 3, 'DARIUS THE GREAT IS NOT OKAY', 'ADIB KHORRAM', '1607158291_DARIUS THE GREAT IS NOT OKAY.png', 2, NULL, '2020-12-05 08:51:31', NULL),
	(5, 16, 1, 'MORE TO LIVE', 'RESHONDA TATE', '1607158342_MORE TO LIVE.png', 4, NULL, '2020-12-05 08:52:22', NULL),
	(6, 8, 1, 'EVERYTING INSIDE', 'EDWIDGE DANTICAT', '1607158383_EVERYTING INSIDE.png', 3, NULL, '2020-12-05 08:53:03', NULL);
/*!40000 ALTER TABLE `tb_buku` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_kategori
CREATE TABLE IF NOT EXISTS `tb_kategori` (
  `id_kategori` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_kategori: ~22 rows (approximately)
DELETE FROM `tb_kategori`;
/*!40000 ALTER TABLE `tb_kategori` DISABLE KEYS */;
INSERT INTO `tb_kategori` (`id_kategori`, `nama_kat`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'Novel', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(2, 'Cergam', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(3, 'Komik', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(4, 'Ensiklopedia', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(5, 'Nomik', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(6, 'Antologi', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(7, 'Dongeng', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(8, 'Biografi', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(9, 'Novelet', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(10, 'Fotografi', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(11, 'Karya ilmiah', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(12, 'Tafsir', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(13, 'Kamus', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(14, 'Panduan (how to)', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(15, 'Atlas', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(16, 'Ilmiah', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(17, 'Teks', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(18, 'Mewarnai', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(19, 'Catatan harian (jurnal/diary)', NULL, '2020-12-04 12:44:26', '2020-12-04 12:44:26'),
	(39, 'tesa', '2020-12-05 20:06:53', NULL, NULL),
	(40, 'tesa', '2020-12-04 13:38:17', NULL, NULL),
	(41, 'tesa', '2020-12-04 13:38:45', NULL, NULL);
/*!40000 ALTER TABLE `tb_kategori` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_lokasi
CREATE TABLE IF NOT EXISTS `tb_lokasi` (
  `id_lokasi` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kode_lokasi` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loker` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_lokasi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_lokasi: ~3 rows (approximately)
DELETE FROM `tb_lokasi`;
/*!40000 ALTER TABLE `tb_lokasi` DISABLE KEYS */;
INSERT INTO `tb_lokasi` (`id_lokasi`, `kode_lokasi`, `loker`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'A001', 'A1', NULL, NULL, NULL),
	(2, 'A001', 'A2', '2020-12-05 04:10:58', NULL, NULL),
	(3, 'A002', 'A1', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tb_lokasi` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_peminjaman
CREATE TABLE IF NOT EXISTS `tb_peminjaman` (
  `id_pinjam` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `jumlah_pinjam` int(11) DEFAULT NULL,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_peminjaman: ~0 rows (approximately)
DELETE FROM `tb_peminjaman`;
/*!40000 ALTER TABLE `tb_peminjaman` DISABLE KEYS */;
INSERT INTO `tb_peminjaman` (`id_pinjam`, `id_transaksi`, `id_buku`, `tgl_pinjam`, `jumlah_pinjam`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2020-12-05', NULL, 'Dikembalikan.', NULL, '2020-12-05 20:17:19', NULL),
	(2, 1, 2, '2020-12-05', NULL, 'Dikembalikan.', NULL, '2020-12-05 20:17:19', NULL),
	(3, 1, 3, '2020-12-05', NULL, 'Dikembalikan.', NULL, '2020-12-05 20:17:19', NULL),
	(4, 2, 1, '2020-12-05', NULL, 'Dikembalikan.', NULL, '2020-12-05 20:21:50', NULL),
	(5, 2, 4, '2020-12-05', NULL, 'Dikembalikan.', NULL, '2020-12-05 20:21:50', NULL),
	(6, 3, 6, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:22:10', NULL),
	(7, 4, 2, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:22:47', NULL),
	(8, 4, 5, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:22:47', NULL),
	(9, 5, 3, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:23:14', NULL),
	(10, 6, 1, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:23:35', NULL),
	(11, 6, 6, '2020-12-05', NULL, 'Dipinjam.', NULL, '2020-12-05 20:23:35', NULL);
/*!40000 ALTER TABLE `tb_peminjaman` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_pengembalian
CREATE TABLE IF NOT EXISTS `tb_pengembalian` (
  `id_kembali` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_pinjam` int(11) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `jumlah_kembali` int(11) DEFAULT NULL,
  `denda` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_kembali`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_pengembalian: ~0 rows (approximately)
DELETE FROM `tb_pengembalian`;
/*!40000 ALTER TABLE `tb_pengembalian` DISABLE KEYS */;
INSERT INTO `tb_pengembalian` (`id_kembali`, `id_pinjam`, `tgl_kembali`, `jumlah_kembali`, `denda`, `created_at`, `updated_at`) VALUES
	(1, 1, '2020-12-08', NULL, NULL, '2020-12-05 20:23:49', NULL),
	(2, 2, '2020-12-08', NULL, NULL, '2020-12-05 20:23:49', NULL),
	(3, 3, '2020-12-08', NULL, NULL, '2020-12-05 20:23:49', NULL),
	(4, 4, '2020-12-10', NULL, NULL, '2020-12-05 20:23:56', NULL),
	(5, 5, '2020-12-10', NULL, NULL, '2020-12-05 20:23:56', NULL);
/*!40000 ALTER TABLE `tb_pengembalian` ENABLE KEYS */;

-- Dumping structure for table galuh-library.tb_transaksi
CREATE TABLE IF NOT EXISTS `tb_transaksi` (
  `id_transaksi` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tgl_jatuh_tempo` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.tb_transaksi: ~0 rows (approximately)
DELETE FROM `tb_transaksi`;
/*!40000 ALTER TABLE `tb_transaksi` DISABLE KEYS */;
INSERT INTO `tb_transaksi` (`id_transaksi`, `id_anggota`, `id_user`, `tgl_transaksi`, `tgl_jatuh_tempo`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 5, 1, '2020-12-05', '2020-12-10', NULL, '2020-12-05 20:17:19', NULL),
	(2, 33, 1, '2020-12-05', '2020-12-08', NULL, '2020-12-05 20:21:50', NULL),
	(3, 21, 1, '2020-12-05', '2020-12-11', NULL, '2020-12-05 20:22:10', NULL),
	(4, 7, 1, '2020-12-05', '2020-12-09', NULL, '2020-12-05 20:22:47', NULL),
	(5, 23, 1, '2020-12-05', '2020-12-09', NULL, '2020-12-05 20:23:14', NULL),
	(6, 8, 1, '2020-12-05', '2020-12-10', NULL, '2020-12-05 20:23:35', NULL);
/*!40000 ALTER TABLE `tb_transaksi` ENABLE KEYS */;

-- Dumping structure for table galuh-library.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table galuh-library.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
